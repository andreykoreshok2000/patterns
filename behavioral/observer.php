<?php

//class Worker implements SplSubject
//{
//
//    private SplObjectStorage $splObjectStorage;
//
//    private string $name = '';
//
//    /**
//     * Worker constructor.
//     * @param SplObjectStorage $splObjectStorage
//     */
//    public function __construct()
//    {
//        $this->splObjectStorage = new SplObjectStorage();
//    }
//
//    public function attach(SplObserver $observer)
//    {
//        $this->splObjectStorage->attach($observer);
//    }
//
//    public function detach(SplObserver $observer)
//    {
//        $this->splObjectStorage->detach($observer);
//    }
//
//    public function notify()
//    {
//        foreach ($this->splObjectStorage as $splObserve) {
//            $splObserve->update($this);
//        }
//    }
//
//    public function changeName($name)
//    {
//        $this->name = $name;
//        $this->notify();
//    }
//
//}
//
//class WorkerObserver implements SplObserver
//{
//    private array $workers = [];
//
//    /**
//     * @return array
//     */
//    public function getWorkers(): array
//    {
//        return $this->workers;
//    }
//
//    public function update(SplSubject $subject)
//    {
//        $this->workers[] = clone $subject;
//    }
//}
//
//
//$observer = new WorkerObserver();
//$worker = new Worker();
//$worker->attach($observer);
//$worker->changeName('Boris');
//$worker->changeName('Bob');
//var_dump($observer->getWorkers());
