<?php

//interface State
//{
//    public function toNext(Task $task);
//
//    public function getStatus();
//}
//
//class Task
//{
//    private State $state;
//
//    /**
//     * @return State
//     */
//    public function getState()
//    {
//        return $this->state;
//    }
//
//    /**
//     * @param State $state
//     */
//    public function setState($state)
//    {
//        $this->state = $state;
//    }
//
//    public function proceedToNext()
//    {
//        $this->state->toNext($this);
//    }
//
//    public static function make(): Task
//    {
//        $self = new self();
//
//        $self->setState(new Created());
//
//        return $self;
//    }
//
//}
//
//class Created implements State
//{
//
//    public function toNext(Task $task)
//    {
//        $task->setState(new Process());
//    }
//
//    public function getStatus()
//    {
//        return __CLASS__;
//    }
//}
//
//class Process implements State
//{
//
//    public function toNext(Task $task)
//    {
//        $task->setState(new Test());
//    }
//
//    public function getStatus()
//    {
//        return __CLASS__;
//    }
//}
//
//class Test implements State
//{
//
//    public function toNext(Task $task)
//    {
//        $task->setState(new Done());
//    }
//
//    public function getStatus()
//    {
//        return __CLASS__;
//    }
//}
//
//class Done implements State
//{
//
//    public function toNext(Task $task)
//    {
//        // TODO: Implement toNext() method.
//    }
//
//    public function getStatus()
//    {
//        return __CLASS__;
//    }
//}
//
//$task = Task::make();
//$task->proceedToNext();
//$task->proceedToNext();
//$task->proceedToNext();
//var_dump($task->getState()->getStatus());
