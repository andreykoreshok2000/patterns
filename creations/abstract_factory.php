<?php

//interface AbstractFactory
//{
//    public static function makeDeveloperWorker(): DeveloperWorker;
//    public static function makeDesignerWorker(): DesignerWorker;
//}
//
//class OutsourceWorkerFactory implements AbstractFactory
//{
//
//    public static function makeDeveloperWorker(): DeveloperWorker
//    {
//        return new OutsourceDeveloperWorker();
//    }
//
//    public static function makeDesignerWorker(): DesignerWorker
//    {
//        return new OutsourceDesignerWorker();
//    }
//}
//
//class NativeWorkerFactory implements AbstractFactory
//{
//
//    public static function makeDeveloperWorker(): DeveloperWorker
//    {
//        return new NativeDeveloperWorker();
//    }
//
//    public static function makeDesignerWorker(): DesignerWorker
//    {
//        return new NativeDesignerWorker();
//    }
//}
//
//interface Worker
//{
//    public function work();
//}
//
//interface DeveloperWorker extends Worker
//{
//
//}
//
//interface DesignerWorker extends Worker
//{
//
//}
//
//class NativeDeveloperWorker implements DeveloperWorker
//{
//    public function work()
//    {
//        return 'developer native';
//    }
//}
//
//class OutsourceDeveloperWorker implements DeveloperWorker
//{
//    public function work()
//    {
//        return 'developer outsource';
//    }
//}
//
//class NativeDesignerWorker implements DesignerWorker
//{
//    public function work()
//    {
//        return 'designer native';
//    }
//}
//
//class OutsourceDesignerWorker implements DesignerWorker
//{
//    public function work()
//    {
//        return 'designer outsource';
//    }
//}
//
//$nativeDeveloperWorker = NativeWorkerFactory::makeDeveloperWorker();
//$nativeDesignerWorker = NativeWorkerFactory::makeDesignerWorker();
//$outsourceDeveloperWorker = OutsourceWorkerFactory::makeDeveloperWorker();
//$outsourceDesignerWorker = OutsourceWorkerFactory::makeDesignerWorker();
//
//var_dump($outsourceDesignerWorker->work());
