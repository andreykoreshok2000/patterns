<?php

//interface Worker
//{
//    public function work();
//}
//
//class Developer implements Worker
//{
//
//    public function work()
//    {
//        return 'Developer';
//    }
//}
//
//class Designer implements Worker
//{
//    public function work()
//    {
//        return 'Designer';
//    }
//}
//
//interface WorkerFactory
//{
//    public static function make();
//}
//
//class DeveloperFactory implements WorkerFactory
//{
//
//    public static function make()
//    {
//        return new Developer();
//    }
//}
//
//class DesignerFactory implements WorkerFactory
//{
//
//    public static function make()
//    {
//        return new Designer();
//    }
//}
//
//$developer = DeveloperFactory::make();
//$designer = DesignerFactory::make();
//
//var_dump($developer);
//var_dump($designer);