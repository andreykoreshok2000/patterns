<?php

//interface Worker
//{
//    public function work();
//}
//
//class Developer implements Worker
//{
//    public function work()
//    {
//        return 'Developer';
//    }
//}
//
//class Designer implements Worker
//{
//    public function work()
//    {
//        return 'Designer';
//    }
//}
//
//class WorkerFactory
//{
//    public static function make($workerTitle): ?Worker
//    {
//        $className = strtoupper($workerTitle);
//
//        if (class_exists($className)) {
//            return new $className();
//        }
//
//        return null;
//    }
//}
//
//$developer = WorkerFactory::make('developer');
//
//var_dump($developer);