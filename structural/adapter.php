<?php
//
//interface NativeWorker
//{
//    public function countSalary(): int;
//}
//
//interface OutsourceWorker
//{
//    public function countSalaryByHour($hour);
//}
//
//class NativeDeveloper implements NativeWorker
//{
//
//    public function countSalary(): int
//    {
//        return 1000 * 20;
//    }
//}
//
//class OutSourceDeveloper implements OutsourceWorker
//{
//
//    public function countSalaryByHour($hour): int
//    {
//        return $hour * 500;
//    }
//}
//
//class OutsourceWorkerAdapter implements NativeWorker
//{
//    private OutsourceWorker $outsourceWorker;
//
//    /**
//     * OutsourceAdapter constructor.
//     * @param OutsourceWorker $outsourceWorker
//     */
//    public function __construct(OutsourceWorker $outsourceWorker)
//    {
//        $this->outsourceWorker = $outsourceWorker;
//    }
//
//    public function countSalary(): int
//    {
//        return $this->outsourceWorker->countSalaryByHour(80);
//    }
//}
//
//$nativeDeveloper = new NativeDeveloper();
//$outsourceDeveloper = new OutSourceDeveloper();
//
//$outsourceWorkerAdapter = new OutsourceWorkerAdapter($outsourceDeveloper);
//
//var_dump($outsourceWorkerAdapter->countSalary());