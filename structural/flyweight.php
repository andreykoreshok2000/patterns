<?php

//interface Mail
//{
//    public function render();
//}
//
//abstract class TypeMail
//{
//    private static $text;
//
//    /**
//     * TypeMail constructor.
//     */
//    public function __construct(string $text)
//    {
//        $this->text = $text;
//    }
//
//    public function getText()
//    {
//        return $this->text;
//    }
//}
//
//class Business extends TypeMail implements Mail
//{
//
//    public function render(): string
//    {
//        return $this->getText() . 'from Business';
//    }
//}
//
//class Job extends TypeMail implements Mail
//{
//
//    public function render(): string
//    {
//        return $this->getText() . 'from Job';
//    }
//}
//
//class MailFactory
//{
//    private array $pool = [];
//
//    public function getMail($id, $typeMail): Mail
//    {
//        if (!isset($this->pool[$id])) {
//            $this->pool[$id] = $this->make($typeMail);
//        }
//
//        return $this->pool[$id];
//    }
//
//    private function make($typeMail)
//    {
//        if ($typeMail === 'business') {
//            return new Business('Business mail text');
//        }
//
//        return new Job('job mail text');
//    }
//}
//
//$mailFactory = new MailFactory();
//$mail = $mailFactory->getMail(12, 'business');
//var_dump($mail->render());
